/* global require */
module.exports = function(grunt) {
	'use strict';

	require( 'load-grunt-tasks' )(grunt);

	// Fire up "grunt watch"
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Fire up WP Readme to Markdown (converts readme.txt to readme.md)
	grunt.loadNpmTasks('grunt-wp-readme-to-markdown');

	// Fire up WP i18n
	grunt.loadNpmTasks( 'grunt-wp-i18n' );

	grunt.initConfig({

		// Adds values from package.json and adds to "pkg" variable
		pkg: grunt.file.readJSON( 'package.json' ),

		wp_readme_to_markdown: {
			your_target: {
				files: {
					'README.md': 'readme.txt'
				},
			},
		},

		// Configure "watch"
		watch: {
			php: {
				files: ['**/*.php'],
				tasks: ['phplint']
			}
		},
		// Configure "phplint"
		phplint: {
			all: ['**/*.php']
		},
		// Configure .php files for code standards
		phpcs: {
			all: {
				dir: ['**/*.php']
			},
			options: {
				standard: 'ruleset.xml',
				reportFile: 'phpcs.txt',
				ignoreExitCode: true
			}
		},
		// JavaScript

		// Lint JS code practices
		jshint: {
			grunt: {
				options: {
					jshintrc: '.gruntjshintrc'
				},
				src: ['Gruntfile.js']
			},
			theme: {
				options: {
					jshintrc: true
				},
				expand: true,
				src: [
					'modules/**/*.js'
				]
			}
		},
		// Lint JS for code standards
		jscs: {
			options: {
				config: '.jscsrc'
			},
			all: {
				files: {
					src: [
						'.gruntjshintrc',
						'.jshintrc',
						'Gruntfile.js',
						'package.json',
						'assets/**/*.js'
					]
				}
			}
		},
		// Lint JSON files for syntax errors
		jsonlint: {
			all: {
				src: [
					'.gruntjshintrc',
					'.jshintrc',
					'package.json'
				]
			}
		},
		// Lint .js files for syntax errors
		jsvalidate: {
			all: {
				options: {
					verbose: true
				},
				files: {
					src: [
						'Gruntfile.js',
						'theme/**/*.js'
					]
				}
			}
		},
		// Images

		// Optimize images to save bytes
		imagemin: {
			dynamic: {
				files: [{
					expand: true,                     // Enable dynamic expansion
					cwd: 'assets/images/',            // Src matches are relative to this path
					src: ['**/*.{png,jpg,gif}'],      // Actual patterns to match
					dest: 'assets/images/'            // Destination path prefix
				}]
			}
		},
		checktextdomain: {
			options: {
				text_domain: 'forsite-security-extension-pack',
				keywords: [
					'__:1,2d',
					'_e:1,2d',
					'_x:1,2c,3d',
					'_ex:1,2c,3d',
					'_n:1,2,4d',
					'_nx:1,2,4c,5d',
					'_n_noop:1,2,3d',
					'_nx_noop:1,2,3c,4d',
					'esc_attr__:1,2d',
					'esc_html__:1,2d',
					'esc_attr_e:1,2d',
					'esc_html_e:1,2d',
					'esc_attr_x:1,2c,3d',
					'esc_html_x:1,2c,3d'
				]
			},
			files: {
				expand: true,
				src: [
					'**/*.php'
				]
			}
		},
		// Configure makepot - builds language .pot file
		makepot: {
			target: {
				options: {
					domainPath: '/languages',
					potFilename: 'security-extension-pack.pot',
					processPot: function( pot, options ) {
						pot.headers['report-msgid-bugs-to'] = 'http://forsitemedia.nl\n';
						pot.headers['plural-forms'] = 'nplurals=2; plural=n != 1;';
						pot.headers['last-translator'] = 'Remkus de Vries <translations@forsitemedia.nl>\n';
						pot.headers['language-team'] = 'Forsite Media <remkus@forsitemedia.nl>\n';
						pot.headers['x-generator'] = 'CSL v1.x\n';
						pot.headers['x-poedit-basepath'] = '.\n';
						pot.headers['x-poedit-language'] = 'English\n';
						pot.headers['x-poedit-country'] = 'UNITED STATES\n';
						pot.headers['x-poedit-sourcecharset'] = 'utf-8\n';
						pot.headers['x-poedit-keywordslist'] = '__;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;\n';
						pot.headers['x-poedit-bookmarks'] = '\n';
						pot.headers['x-poedit-searchpath-0'] = '.\n';
						pot.headers['x-textdomain-support'] = 'yes\n';
						return pot;
					},
					type: 'wp-plugin'
				}
			}
		}
		// Register tasks
	});
};