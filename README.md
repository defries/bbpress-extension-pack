# bbPress Extension Pack #
**Contributors:** forsitemedia  
**Tags:** bbpress, notifications  
**Requires at least:** 4.2
**Tested up to:** 4.2
**Stable tag:** 0.1
**License:** GPLv2


Adds extra functionality to bbPress.


## Description ##

Adds extra functionality to bbPress.


## Installation ##

1. coming soon


## Changelog ##

### 0.1 ###
* Initial alpha
