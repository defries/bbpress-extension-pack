<?php
/*
Plugin Name: bbPress Extension Pack
Plugin URI: https://github.com/forsitemedia/bbpress-extension-pack
Description: Adds extra bbPress functionality
Author: Forsite Themes
Version: 0.1
Author URI: http://forsitethemes.com/

License: GPLv2 ->

  Copyright 2012-2014 Forsite Themes (team@forsitethemes.com)

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License, version 2, as
  published by the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

*/


/*
 * Load the Extension pack framework
 */
if ( ! class_exists( 'Forsite_Extension_Pack' ) ) {
//	require( 'extension-pack/lib/custom-meta-boxes/custom-meta-boxes.php' );
	require( 'extension-pack/extension-framework.php' );
}

/*
 * Load the module pack
 * Utilizes the Forsite Extension Pack framework for handling individual modules
 */
class Forsite_bbPress_Extension extends Forsite_Extension_Pack {

	public $plugin_name = 'security-extension-pack/plugin.php';
	public $option_name = 'bbpep_modules';
	public $page_name = 'display_bbpep_admin_page';
	public $admin_url;

	public $pack_dir;
	public $pack_url;
	public $modules_dir;
	public $modules_url;

	public $nonce = 'bbpep-nonce';
	public $page_title;
	public $main_menu_title;
	public $main_menu_icon;
	public $main_menu_position = '82'; // This needs to be unique

	public $bulk_editor_page_title;
	public $bulk_editor_menu_title;
	public $bulk_editor_page_name = 'display_bbpep_modules_page';
	public $module_defaults;

	/**
	 * Initializes the plugin by setting localization, filters, and administration functions.
	 */
	function __construct() {

		// Set variables used by the extension pack framework
		$this->pack_dir               = dirname( __FILE__ );
		$this->pack_url               = plugin_dir_url( __FILE__ );
		$this->modules_dir            = dirname( __FILE__ ) . '/modules';
		$this->modules_url            = plugin_dir_url( __FILE__ ) . '/modules/';

		$this->page_title             = __( 'bbPress', 'bbpress-extension-pack' );
		$this->main_menu_title        = __( 'bbPress', 'bbpress-extension-pack' );
		$this->main_menu_icon         = ''; // Using CSS instead

		$this->bulk_editor_page_title = __( 'Modules', 'bbpress-extension-pack' );
		$this->bulk_editor_menu_title = __( 'Modules', 'bbpress-extension-pack' );

		add_action( 'admin_head',     array( $this, 'admin_css' ) );
		add_action( 'plugins_loaded', array( $this, 'module_defaults' ) );

		// Set admin URL
		if ( isset( $_GET['page'] ) && ( $this->bulk_editor_page_name == $_GET['page'] || $this->page_name == $_GET['page'] ) ) {
			$slug = $_GET['page'];
			if ( is_network_admin() ) {
				$this->admin_url = network_admin_url() . 'admin.php?page=' . $slug;
			} else {
				$this->admin_url = admin_url() . 'admin.php?page=' . $slug;
			}
			$this->admin_url = esc_url( $this->admin_url );
		}

		// Load text string
		load_plugin_textdomain( 'bbpress-extension-pack', false, $this->pack_url . '/assets/languages' );

		// Load the Extension Pack's contructor
		parent::__construct();

	}

	/**
	 * Adding bbPress icon to main admin menu link.
	 */
	public function admin_css() {
		echo '
		<style>
		#adminmenu #toplevel_page_display_bbpep_admin_page .wp-menu-image:before {
			content: "\f449";
		}
		</style>';
	}

	/**
	 * Sets the details of modules that are available.
	 * The values below are considered the defaults, and are overridden by what is stored
	 * in the bbpep_modules setting
	 */
	public function module_defaults() {

		$module_defaults = array(
			0 => array(
				'show_image' => true,
				'id' => $id = 'bbPress-Custom-Reply-Notifications',
				'name' => __( 'Custom reply notifications', 'bbpress-extension-pack' ),
				'description' => __( 'Customize the email sent to forum & topic subscribers when a new topic or reply is posted.', 'bbpress-extension-pack' ),
				'admin_settings' => '',
				'author' => $author = 'ForSite Media',
				'authorAndUri' => '<a href="https://forsite.media/" title="' . esc_attr( $author ) . '">' . $author . '</a>',
				'version' => '1.0',
				'tags' => 'popular',
				'introduced' => '1.0',
			),
		);

		// Set variable - provide filtering for other plugins to manually set values and remove modules when necessary
		$this->module_defaults = apply_filters( 'fs_bbpress_module_defaults', $module_defaults );

	}

}

new Forsite_bbPress_Extension(); // Fire her up, baby!
